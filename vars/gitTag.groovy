def call(tag, message = null) {
  // if no message is passed via param, we craft a default one that universal
  if(message == null) {
    message = "Release ${tag}"
  }

  sh "git tag ${tag} -m '${message}'"
}
