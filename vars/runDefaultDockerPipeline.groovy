def call(Map params) {
  if(params.imageName == null) {
    throw new IllegalArgumentException("imageName is required!")
  }

  if(params.currentBuild == null) {
    throw new IllegalArgumentException("currentBuild is required!")
  }

  def image
  def tag

  stage("Checkout") {
    checkout scm
    configureGitCommiter()
    def credentials = params.gitCredentials ?: "github-timo-reymann"
    configureGitPushUrl(credentials)
    checkoutBranch env.BRANCH_NAME
  }

  stage("Get and increase tag") {
    tag = params.tag ?: increaseMinorVersion(getLastTag())
    params.currentBuild.description = tag
  }

  stage("Build") {
    image = docker.build(params.imageName, "--pull .")
  }

  stage("Publish") {
    docker.withRegistry(params.dockerRegistry ?: "https://registry.hub.docker.com", params.dockerCredentials ?: "timoreymann-docker") {
      dockerRelease(image, tag)
    }
  }

  stage("Tag") {
    gitTag(tag)
    gitPushAll()
  }
}
