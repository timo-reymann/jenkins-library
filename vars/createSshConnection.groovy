def call(host, credentialsId) {
   def remote = [:]
   remote.name = host.split("\\.")[0]
   remote.host = host
   remote.allowAnyHosts = true
   
   withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'password', usernameVariable: 'username')]) {
      remote.user = username
      remote.password = password
   }

   return remote
}
