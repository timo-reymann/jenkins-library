def call() {
   def packageJSON = readJSON file: 'package.json'
   return packageJSON.version
}
