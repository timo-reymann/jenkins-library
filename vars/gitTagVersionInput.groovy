def call(label = 'Version', description = 'Version to release') {
    return new com.cwctravel.hudson.plugins.extended_choice_parameter.ExtendedChoiceParameterDefinition(
                label,
                'PT_TEXTBOX',
                null,
                null,
                "",
                "",
                "",
                "",
                "",
                "" ,
                "",
                "",
                """
                    def processBuilder = new ProcessBuilder(["sh", "-c", "git tag --list --sort=v:refname | tail -1"])
                    processBuilder.directory(new File("${env.WORKSPACE}"))
                    def tag
                    try {
                        tag = processBuilder.start().text
                    } catch (e) {
                        return e.toString()
                    }

                    def versionParts = tag.tokenize(".")
                    if(versionParts.size != 3) {
                        return "0.0.1"
                    }
                    def minor = versionParts[1].toInteger() + 1;
                    return versionParts[0] + "." + minor + ".0"
                """,
                "",
                "",
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                false,
                false,
                20,
                description,
                ","
    )
}
