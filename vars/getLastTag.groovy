def call(defaultValue = "0.1.0") {
  def tag = sh script: "git tag | sort -n | tail -1", returnStdout: true

  if(tag.trim().length() == 0) {
    return defaultValue
  }

  return tag.trim()
}
