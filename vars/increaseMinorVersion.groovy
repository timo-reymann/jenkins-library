def call(versionInput) {
    def versionParts = versionInput.tokenize(".")
    if(versionParts.size != 3) {
        throw new IllegalArgumentException("Wrong version format, only MAJOR.MINOR.PATCH is supported!")
    }

    def minor = versionParts[1].toInteger() + 1;

   return "${versionParts[0]}.${minor}.0"
}
