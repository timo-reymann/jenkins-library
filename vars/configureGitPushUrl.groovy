def call(credentialsId = null) {
  if(credentialsId == null) {
    throw new IllegalArgumentException("credentialsId is required!")
  }

  def origin = sh script: "git config --get remote.origin.url", returnStdout: true 
  def originParts = origin.split('//')

  if(originParts.size() != 2) {
    throw new IllegalArgumentException("Invalid remote url: ${origin}, it must be using http(s)!")
  }

  withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'password', usernameVariable: 'username')]) {
    sh "git remote set-url origin ${originParts[0]}//${username}:${password}@${originParts[1]}"
  }
}
