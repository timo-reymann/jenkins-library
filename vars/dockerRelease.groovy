def call(image, version = null) {
   if(version != null) {
      image.push(version)
   }

   image.push("latest")
}
